#!/bin/sh

if [ -f "/etc/privoxy/firstrun" ]; then
	case "$listen" in

	"v4")
		ipv4LocalAddr=$(awk '/32 host/ { print f } {f=$2}' /proc/net/fib_trie | sort | uniq | grep -v 127.0.0.1)
		if [ -z ipv4LocalAddr ]; then
			echo "Error: V4 addr not found"
			sleep 10
			exit 1
		fi
		echo "listen-address $ipv4LocalAddr:8118" | tee  -a /etc/privoxy/config
		;;

	"v6local")
		v6LocalAddr=$(cat /proc/net/if_inet6 | grep eth0 | grep fe80 | cut -d' ' -f1 | sed 's/.\{4\}/&:/g; s/.$//')
		if [ -z v6LocalAddr ]; then
			echo "Error: V6 addr not found"
			sleep 10
			exit 1
		fi
		echo 'listen-address [fe80::%eth0]:8118' | tee  -a /etc/privoxy/config
		;;

	"global")
		echo 'listen-address [::]:8118' | tee  -a /etc/privoxy/config
		;;

	*)
		echo 'listen-address  127.0.0.1:8118' | tee  -a /etc/privoxy/config
		;;
	esac
fi

if [ -z $nameserver ]; then
	echo -e "nameserver 1.1.1.1\nnameserver 2606:4700:4700::1111" | tee  /etc/resolv.conf
else
	echo -e "$nameserver" | tee  /etc/resolv.conf
fi

if [ -z $1 ]; then
	privoxy --no-daemon /etc/privoxy/config
else
	shift 1
	privoxy $@
fi
