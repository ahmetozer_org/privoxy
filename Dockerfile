FROM alpine

RUN apk add privoxy && \
touch /etc/privoxy/firstrun 
COPY config /etc/privoxy/config
COPY privoxy.sh /bin/privoxy.sh
RUN chmod +x /bin/privoxy.sh

ENTRYPOINT privoxy.sh
